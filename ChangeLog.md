# 0.0.3 -- 2022-05-01
- Add Ithacanet and Jakartanet to Tezos_Beacon.Network.Type.

# 0.0.2 -- 2022-03-19
- connectToBeacon takes a new parameter, dappName. It is no longer hard coded.
