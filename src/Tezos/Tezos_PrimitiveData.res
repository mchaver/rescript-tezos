type t =
  | Elt
  | Right
  | False
  | Unit
  | Some
  | None
  | Left
  | True
  | Pair

let encode = (primitiveData: t): Js.Json.t =>
  switch primitiveData {
  | Elt => Json.Encode.string("Elt")
  | Right => Json.Encode.string("Right")
  | False => Json.Encode.string("False")
  | Unit => Json.Encode.string("Unit")
  | Some => Json.Encode.string("Some")
  | None => Json.Encode.string("None")
  | Left => Json.Encode.string("Left")
  | True => Json.Encode.string("True")
  | Pair => Json.Encode.string("Pair")
  }

let decode = (json: Js.Json.t): Belt.Result.t<t, string> =>
  switch Json.Decode.string(json) {
  | str =>
    switch str {
    | "Elt" => Belt.Result.Ok(Elt)
    | "Right" => Belt.Result.Ok(Right)
    | "False" => Belt.Result.Ok(False)
    | "Unit" => Belt.Result.Ok(Unit)
    | "Some" => Belt.Result.Ok(Some)
    | "None" => Belt.Result.Ok(None)
    | "Left" => Belt.Result.Ok(Left)
    | "True" => Belt.Result.Ok(True)
    | "Pair" => Belt.Result.Ok(Pair)
    | _ => Belt.Result.Error("PrimitiveData.decode: " ++ str)
    }
  | exception Json.Decode.DecodeError(error) => Belt.Result.Error("PrimitiveData.decode: " ++ error)
  }
