exception DecodeError(string)

module Network = {
  module Type = {
    module Raw = {
      type t = string
    }

    type t =
      | Mainnet
      | Hangzhounet
      | Ithacanet
      | Jakartanet
      | Custom

    let fromRaw = (raw: Raw.t): t =>
      switch raw {
      | "mainnet" => Mainnet
      | "hangzhounet" => Hangzhounet
      | "ithacanet" => Ithacanet
      | "jakartanet" => Jakartanet
      | "custom" => Custom
      | _ => \"@@"(raise, DecodeError("Expected Network.Type, got " ++ raw))
      }

    let toRaw = (t: t): Raw.t =>
      switch t {
      | Mainnet => "mainnet"
      | Hangzhounet => "hangzhounet"
      | Ithacanet => "ithacanet"
      | Jakartanet => "jakartanet"
      | Custom => "custom"
      }
  }

  module Raw = {
    type t = {
      @as("type")
      type_: string,
      @as("name")
      name: Js.Nullable.t<string>,
      @as("rpcUrl")
      rpcUrl: Js.Nullable.t<string>,
    }
  }

  type t = {
    type_: Type.t,
    name: option<string>,
    rpcUrl: option<string>,
  }

  let fromRaw = (raw: Raw.t): t => {
    type_: Type.fromRaw(raw.type_),
    name: Js.Nullable.toOption(raw.name),
    rpcUrl: Js.Nullable.toOption(raw.rpcUrl),
  }

  let toRaw = (t: t): Raw.t => {
    type_: Type.toRaw(t.type_),
    name: t.name |> Js.Nullable.fromOption,
    rpcUrl: t.rpcUrl |> Js.Nullable.fromOption,
  }
}

module Permission = {
  module Scope = {
    module Raw = {
      type t = string
    }

    type t =
      | OperationRequest
      | Sign
      | Threshold

    let fromRaw = (raw: Raw.t): t =>
      switch raw {
      | "operation_request" => OperationRequest
      | "sign" => Sign
      | "threshold" => Threshold
      | _ => \"@@"(raise, DecodeError("Expected Permissions.Scope, got " ++ raw))
      }
  }

  module Response = {
    module Raw = {
      type t = {
        @as("beaconId")
        beaconId: string,
        @as("address")
        address: string,
        @as("network")
        network: Network.Raw.t,
        @as("scopes")
        scopes: array<string>,
      }
    }

    type t = {
      beaconId: string,
      address: string,
      network: Network.t,
      scopes: list<Scope.t>,
    }

    let fromRaw = (raw: Raw.t): t => {
      beaconId: raw.beaconId,
      address: raw.address,
      network: Network.fromRaw(raw.network),
      scopes: Array.map(Scope.fromRaw, raw.scopes) |> Array.to_list,
    }
  }
}

module AccountInfo = {
  module Raw = {
    /* [@bs.deriving {abstract: light}] */
    type t = {
      @as("accountIdentifier")
      accountIdentifier: string,
      @as("beaconId")
      beaconId: string,
      @as("address")
      address: string,
      @as("pubkey")
      pubkey: string,
      @as("network")
      network: Network.Raw.t,
      @as("scopes")
      scopes: array<Permission.Scope.Raw.t>,
    }
  }

  type t = {
    accountIdentifier: string,
    beaconId: string,
    address: string,
    pubkey: string,
    network: Network.t,
    scopes: list<Permission.Scope.t>,
  }

  let fromRaw = (raw: Raw.t): t => {
    accountIdentifier: raw.accountIdentifier,
    beaconId: raw.beaconId,
    address: raw.address,
    pubkey: raw.pubkey,
    network: raw.network |> Network.fromRaw,
    scopes: Array.map(Permission.Scope.fromRaw, raw.scopes) |> Array.to_list,
  }

  let hasWrite = t =>
    List.mem(Permission.Scope.OperationRequest, t.scopes) &&
    List.mem(Permission.Scope.Sign, t.scopes)
}

module Tezos = {
  module Transaction = {
    module Parameters = {
      module Raw = {
        @deriving({abstract: light})
        type t = {
          @as("entrypoint")
          entrypoint: string,
          @as("value")
          value: Js.Json.t,
        }
      }

      type t = {
        entrypoint: string,
        value: Tezos_Expression.t,
      }

      let toRaw = (t: t): Raw.t =>
        Raw.t(~entrypoint=t.entrypoint, ~value=Tezos_Expression.encode(t.value))
    }
  }

  module Operation = {
    module Response = {
      module Raw = {
        type t = {
          @as("beaconId")
          beaconId: string,
          @as("transactionHash")
          transactionHash: string,
        }
      }

      type t = {
        beaconId: string,
        transactionHash: string,
      }

      let fromRaw = (raw: Raw.t): t => {
        beaconId: raw.beaconId,
        transactionHash: raw.transactionHash,
      }
    }

    module Type = {
      type t =
        | Endorsement
        | SeedNonceRevelation
        | DoubleEndorsementEvidence
        | DoubleBakingEvidence
        | ActivateAccount
        | Proposals
        | Ballot
        | Reveal
        | Transaction
        | Origination
        | Delegation

      let toString = (t: t): string =>
        switch t {
        | Endorsement => "endorsement"
        | SeedNonceRevelation => "seed_nonce_revelation"
        | DoubleEndorsementEvidence => "double_endorsement_evidence"
        | DoubleBakingEvidence => "double_baking_evidence"
        | ActivateAccount => "activate_account"
        | Proposals => "proposals"
        | Ballot => "ballot"
        | Reveal => "reveal"
        | Transaction => "transaction"
        | Origination => "origination"
        | Delegation => "delegation"
        }
    }

    module Raw = {
      @deriving({abstract: light})
      type t = {
        @as("kind")
        kind: string,
        @as("amount")
        amount: Js.Json.t,
        @as("destination")
        destination: Js.Json.t,
        @as("parameters")
        parameters: Js.Nullable.t<Transaction.Parameters.Raw.t>,
      }
    }

    type t = {
      kind: Type.t,
      amount: Tezos_Mutez.t,
      destination: Tezos_Contract.t /* it can be contract or address */,
      parameters: option<Transaction.Parameters.t>,
    }

    let toRaw = (t: t): Raw.t =>
      Raw.t(
        ~kind=Type.toString(t.kind),
        ~amount=Tezos_Mutez.encode(t.amount),
        ~destination=Tezos_Contract.encode(t.destination),
        ~parameters=switch t.parameters {
        | Some(p) => Js.Nullable.return(Transaction.Parameters.toRaw(p))
        | None => Js.Nullable.null
        },
      )
  }
}

type dappClient

let createDappClientRaw: string => dappClient = %raw(`
        function(dapp) {
          const beacon = require("@airgap/beacon-sdk");
          return new beacon.DAppClient({name: dapp});
        }
       `)

let createDappClient = (name: string): dappClient => createDappClientRaw(name)

let postTransactionRaw: (
  dappClient,
  Network.Raw.t,
  array<Tezos_Operation.Raw.t>,
) => Js.Promise.t<Tezos.Operation.Response.Raw.t> = %raw(`
        function(client, network, operations) {
          console.log(operations);
          return client
                .requestOperation({
                  network: network,
                  operationDetails: operations
                })
                .then(function (response) {
                  console.log('operations', response)
                  return response;
                })
                .catch(function (operationError) {console.error(operationError); return operationError;});
         }
       `)

let requestPermissionsRaw: (
  dappClient,
  Network.Raw.t,
) => Js.Promise.t<Permission.Response.Raw.t> = %raw(`
        function(client, network) {
          return client.requestPermissions({network: network});
         }
       `)

let requestPermissions = (dappClient, network): Js.Promise.t<Permission.Response.t> =>
  requestPermissionsRaw(dappClient, Network.toRaw(network)) |> Js.Promise.then_(response =>
    Permission.Response.fromRaw(response) |> Js.Promise.resolve
  )

@send
external requestOperation: (dappClient, Tezos.Operation.Raw.t) => Js.Promise.t<unit> =
  "requestOperation"

let getActiveAccountRaw: dappClient => Js.Promise.t<Js.Nullable.t<AccountInfo.Raw.t>> = %raw(`
        function(client) {
          return client.getActiveAccount();
        }
       `)

let disconnectRaw: dappClient => Js.Promise.t<unit> = %raw(`
        function(client) {
          return client.setActiveAccount(undefined);
        }
       `)

let disconnect = (dappClient): Js.Promise.t<unit> => disconnectRaw(dappClient)

let getActiveAccount = (dappClient): Js.Promise.t<option<AccountInfo.t>> =>
  getActiveAccountRaw(dappClient) |> Js.Promise.then_(account =>
    switch Js.Nullable.toOption(account) {
    | Some(a) => Some(AccountInfo.fromRaw(a))
    | None => None
    } |> Js.Promise.resolve
  )

let postTransaction = (dappClient, operations: list<Tezos_Operation.t>): Js.Promise.t<
  option<Tezos.Operation.Response.t>,
> =>
  getActiveAccount(dappClient) |> Js.Promise.then_(account =>
    switch account {
    | Some(account: AccountInfo.t) =>
      postTransactionRaw(
        dappClient,
        Network.toRaw(account.network),
        Belt.List.map(operations, Tezos_Operation.toRaw) |> Array.of_list,
      ) |> Js.Promise.then_(response =>
        Js.Promise.resolve(Some(Tezos.Operation.Response.fromRaw(response)))
      )
    | None => Js.Promise.resolve(None)
    }
  )

let hasRequiredPermissions = (account: AccountInfo.t) =>
  List.mem(Permission.Scope.OperationRequest, account.scopes) &&
  List.mem(Permission.Scope.Sign, account.scopes)

let rec loginToBeacon = (network: Network.t, client: dappClient, firstAttempt: bool): Js.Promise.t<
  option<(dappClient, AccountInfo.t)>,
> =>
  getActiveAccount(client) |> Js.Promise.then_(account =>
    switch account {
    | None =>
      if firstAttempt {
        requestPermissions(client, network) |> Js.Promise.then_(_permissions =>
          /* user has accepted the permissions but we need to check if they are the correct ones */
          loginToBeacon(network, client, false)
        )
      } else {
        Js.Promise.resolve(None)
      }

    | Some(account: AccountInfo.t) =>
      if hasRequiredPermissions(account) {
        Js.Promise.resolve(Some((client, account)))
      } else if firstAttempt {
        requestPermissions(client, network) |> Js.Promise.then_(_permissions =>
          loginToBeacon(network, client, false)
        )
      } else {
        Js.Promise.resolve(None)
      }
    }
  )

module Tezos_Account = {
  // Tezos address provided by beacon
  // Tezos client for connection to beacon
  // xtz held by account, queried through mt art house server

  type t = {
    address: string, // Tezos_Address.t,
    client: dappClient,
    xtz: Tezos_Mutez.t,
  }
}

let connectToBeacon = (
  dappName: string,
  network: Network.t,
  setAccount: option<Tezos_Account.t> => unit,
) => {
  let client = createDappClient(dappName)
  loginToBeacon(network, client, true)
  |> Js.Promise.then_((response: option<(dappClient, AccountInfo.t)>) =>
    switch response {
    | Some((client, account)) =>
      Js.log("connected to Beacon.")
      Js.log(client)
      Js.log(account)
      setAccount(Some({address: account.address, client: client, xtz: Tezos_Mutez.zero}))
      Js.Promise.resolve()
    | None => Js.log("Unable to get beacon client or account.") |> Js.Promise.resolve
    }
  )
  |> ignore
  Js.Promise.resolve()
}

let getActiveTezosAccount = (): Js.Promise.t<option<Tezos_Account.t>> => {
  let client = createDappClient("MT Art House")
  getActiveAccount(client) |> Js.Promise.then_((account: option<AccountInfo.t>) =>
    switch account {
    | None => Js.Promise.resolve(None)

    | Some(account: AccountInfo.t) =>
      if hasRequiredPermissions(account) {
        Js.Promise.resolve(
          Some(
            ({address: account.address, client: client, xtz: Tezos_Mutez.zero}: Tezos_Account.t),
          ),
        )
      } else {
        Js.Promise.resolve(None)
      }
    }
  )
}
