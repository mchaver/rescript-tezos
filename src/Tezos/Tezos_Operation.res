@ocaml.doc("
 * A generic Tezos operation interface used by different wallets
 * Generally the data passed is the same because it all goes to
 * the node, but the shape of the data may differ between wallet.
 * In that case we need to convert to this type.
 ")
exception DecodeError(string)

module Kind = {
  type t =
    | EndorsementWithSlot
    | Endorsement
    | SeedNonceRevelation
    | DoubleEndorsementEvidence
    | DoubleBakingEvidence
    | ActivateAccount
    | Proposals
    | Ballot
    | Reveal
    | Transaction
    | Origination
    | Delegation

  let toString = (t: t): string =>
    switch t {
    | EndorsementWithSlot => "endorsement_with_slot"
    | Endorsement => "endorsement"
    | SeedNonceRevelation => "seed_nonce_revelation"
    | DoubleEndorsementEvidence => "double_endorsement_evidence"
    | DoubleBakingEvidence => "double_baking_evidence"
    | ActivateAccount => "activate_account"
    | Proposals => "proposals"
    | Ballot => "ballot"
    | Reveal => "reveal"
    | Transaction => "transaction"
    | Origination => "origination"
    | Delegation => "delegation"
    }

  let fromString = (s: string): t =>
    switch s {
    | "endorsement" => Endorsement
    | "seed_nonce_revelation" => SeedNonceRevelation
    | "double_endorsement_evidence" => DoubleEndorsementEvidence
    | "double_baking_evidence" => DoubleBakingEvidence
    | "activate_account" => ActivateAccount
    | "proposals" => Proposals
    | "ballot" => Ballot
    | "reveal" => Reveal
    | "transaction" => Transaction
    | "origination" => Origination
    | "delegation" => Delegation
    | _ => \"@@"(raise, DecodeError("Expected Network.Type, got " ++ s))
    }
}

module Parameters = {
  module Raw = {
    type t = {
      @as("entrypoint")
      entrypoint: string,
      @as("value")
      value: Js.Json.t,
    }
  }

  type t = {
    entrypoint: string,
    value: Tezos_Expression.t,
  }

  let toRaw = (t: t): Raw.t => {
    entrypoint: t.entrypoint,
    value: Tezos_Expression.encode(t.value),
  }
}

module Raw = {
  type t = {
    @as("kind")
    kind: string,
    @as("amount")
    amount: string,
    @as("destination")
    destination: string,
    @as("parameters")
    parameters: Js.Nullable.t<Parameters.Raw.t>,
  }
}

type t = {
  kind: Kind.t,
  amount: Tezos_Mutez.t,
  destination: Tezos_Contract.t,
  parameters: option<Parameters.t>,
}

let toRaw = (t: t): Raw.t => {
  kind: Kind.toString(t.kind),
  amount: Tezos_Mutez.toString(t.amount),
  destination: Tezos_Contract.toString(t.destination),
  parameters: switch t.parameters {
  | Some(p) => Js.Nullable.return(Parameters.toRaw(p))
  | None => Js.Nullable.null
  },
}
