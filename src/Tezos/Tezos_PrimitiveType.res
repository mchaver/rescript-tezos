type t =
  | Timestamp
  | Signature
  | Set
  | Pair
  | Bytes
  | Address
  | Or
  | List
  | Storage
  | KeyHash
  | Unit
  | Option
  | BigMap
  | String
  | Mutez
  | Bool
  | Operation
  | Contract
  | Map
  | Nat
  | Key
  | Lambda
  | Int
  | Parameter
  | Code

let encode = (primitiveType: t): Js.Json.t =>
  switch primitiveType {
  | Timestamp => Json.Encode.string("timestamp")
  | Signature => Json.Encode.string("signature")
  | Set => Json.Encode.string("set")
  | Pair => Json.Encode.string("pair")
  | Bytes => Json.Encode.string("bytes")
  | Address => Json.Encode.string("address")
  | Or => Json.Encode.string("or")
  | List => Json.Encode.string("list")
  | Storage => Json.Encode.string("storage")
  | KeyHash => Json.Encode.string("key_hash")
  | Unit => Json.Encode.string("unit")
  | Option => Json.Encode.string("option")
  | BigMap => Json.Encode.string("big_map")
  | String => Json.Encode.string("string")
  | Mutez => Json.Encode.string("mutez")
  | Bool => Json.Encode.string("bool")
  | Operation => Json.Encode.string("operation")
  | Contract => Json.Encode.string("contract")
  | Map => Json.Encode.string("map")
  | Nat => Json.Encode.string("nat")
  | Key => Json.Encode.string("key")
  | Lambda => Json.Encode.string("lambda")
  | Int => Json.Encode.string("int")
  | Parameter => Json.Encode.string("parameter")
  | Code => Json.Encode.string("code")
  }

let decode = (json: Js.Json.t): Belt.Result.t<t, string> =>
  switch Json.Decode.string(json) {
  | str =>
    switch str {
    | "timestamp" => Belt.Result.Ok(Timestamp)
    | "signature" => Belt.Result.Ok(Signature)
    | "set" => Belt.Result.Ok(Set)
    | "pair" => Belt.Result.Ok(Pair)
    | "bytes" => Belt.Result.Ok(Bytes)
    | "address" => Belt.Result.Ok(Address)
    | "or" => Belt.Result.Ok(Or)
    | "list" => Belt.Result.Ok(List)
    | "storage" => Belt.Result.Ok(Storage)
    | "key_hash" => Belt.Result.Ok(KeyHash)
    | "unit" => Belt.Result.Ok(Unit)
    | "option" => Belt.Result.Ok(Option)
    | "big_map" => Belt.Result.Ok(BigMap)
    | "string" => Belt.Result.Ok(String)
    | "mutez" => Belt.Result.Ok(Mutez)
    | "bool" => Belt.Result.Ok(Bool)
    | "operation" => Belt.Result.Ok(Operation)
    | "contract" => Belt.Result.Ok(Contract)
    | "map" => Belt.Result.Ok(Map)
    | "nat" => Belt.Result.Ok(Nat)
    | "key" => Belt.Result.Ok(Key)
    | "lambda" => Belt.Result.Ok(Lambda)
    | "int" => Belt.Result.Ok(Int)
    | "parameter" => Belt.Result.Ok(Parameter)
    | "code" => Belt.Result.Ok(Code)
    | _ => Belt.Result.Error("PrimitiveType.decode: " ++ str)
    }
  | exception Json.Decode.DecodeError(error) => Belt.Result.Error("PrimitiveType.decode: " ++ error)
  }
