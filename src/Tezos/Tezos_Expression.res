type rec t =
  | IntExpression(Bigint.t)
  | StringExpression(string)
  | BytesExpression(string)
  | Expressions(list<t>)
  | SingleExpression(
      Tezos_Primitives.t /* ** prim */,
      option<list<t>> /* ** args */,
      option<list<string>> /* ** annots */,
    )

let rec encode = (expression: t): Js.Json.t =>
  switch expression {
  | IntExpression(i) =>
    open Json.Encode
    object_(list{("int", string(Bigint.to_string(i)))})
  | StringExpression(str) =>
    open Json.Encode
    object_(list{("string", string(str))})
  | BytesExpression(bytes) =>
    open Json.Encode
    object_(list{("bytes", string(bytes))})
  | Expressions(expressions) => Json.Encode.list(encode, expressions)
  | SingleExpression(prim, args, annots) =>
    let argsEncoded = switch args {
    | Some(args) => list{("args", Json.Encode.list(encode, args))}
    | None => list{}
    }
    let annotsEncoded = switch annots {
    | Some(annots) => list{("annots", Json.Encode.list(Json.Encode.string, annots))}
    | None => list{}
    }
    open Json.Encode
    object_(
      List.concat(list{list{("prim", Tezos_Primitives.encode(prim))}, argsEncoded, annotsEncoded}),
    )
  }

let rec decode = (json: Js.Json.t): Belt.Result.t<t, string> => {
  open Json.Decode
  switch list(a => Tezos_Util.unwrapResult(decode(a)), json) {
  | v => Belt.Result.Ok(Expressions(v))
  | exception DecodeError(_error) =>
    switch field("int", string, json) {
    | v =>
      switch Tezos_Util.bigintOfString(v) {
      | Some(i) => Belt.Result.Ok(IntExpression(i))
      | None => Belt.Result.Error("Expected string encoded int.")
      }
    | exception DecodeError(_error) =>
      switch field("string", string, json) {
      | v => Belt.Result.Ok(StringExpression(v))
      | exception DecodeError(_error) =>
        switch field("bytes", string, json) {
        | v => Belt.Result.Ok(BytesExpression(v))
        | exception DecodeError(_error) =>
          switch (
            field("prim", a => Tezos_Util.unwrapResult(Tezos_Primitives.decode(a)), json),
            optional(field("args", list(a => Tezos_Util.unwrapResult(decode(a)))), json),
            optional(field("annots", list(string)), json),
          ) {
          | (prim, args, annots) => Belt.Result.Ok(SingleExpression(prim, args, annots))
          | exception DecodeError(error) => Belt.Result.Error(error)
          }
        }
      }
    }
  }
}

let expressions = x => Expressions(x)

let singleExpression = (~prim, ~args=?, ~annots=?, ()) => SingleExpression(prim, args, annots)
